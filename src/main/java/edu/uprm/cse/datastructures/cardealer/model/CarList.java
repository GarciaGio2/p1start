package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

public class CarList {
	
	private  static CircularSortedDoublyLinkedList<Car> List = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());
	public static CircularSortedDoublyLinkedList<Car>  getInstance(){
		return List;
	}
	
	public static void resetCars() {
		List = new CircularSortedDoublyLinkedList<Car>(new CarComparator<Car>());
	}

}