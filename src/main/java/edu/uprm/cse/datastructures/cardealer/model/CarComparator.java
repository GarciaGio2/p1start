package edu.uprm.cse.datastructures.cardealer.model;
import java.util.Comparator;

public class CarComparator<E> implements Comparator<E> {

	@Override
	public int compare(E c1, E c2) {
		 if (!(c1 instanceof Car) && !(c2 instanceof Car)) {
			 throw new IllegalStateException("Object is either null or not type Car");
		 }
		 Car car1 = (Car)c1;
		 Car car2 = (Car)c2;
		 
		 if (car1.getCarBrand().compareTo(car2.getCarBrand())  == 0) {
			 if (car1.getCarModel().compareTo(car2.getCarModel()) == 0) {
				 return car1.getCarModelOption().compareTo(car2.getCarModelOption());
			 }
			 else {
				 return car1.getCarModel().compareTo(car2.getCarModel());
			 }
		 }
		 else {
			 return car1.getCarBrand().compareTo(car2.getCarBrand());
		 }
		 
		
		
	}

}