package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.Path;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;
import javax.ws.rs.NotFoundException;

@Path("/cars")
public class CarManager {
	
	//Deletes the person's car, given an ID.
	@DELETE
	@Path("{id}/delete")
	public Response deleteCustomer(@PathParam("id") long id){
		SortedList<Car> List = CarList.getInstance();
		for (int i = 0; i < List.size(); i++) {
			if (List.get(i).getCarId() == id) {
				List.remove(List.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build(); 
	}  
	
	//Changes the person's vehicle given an ID.
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCustomer(Car vehicle){
		SortedList<Car> List = CarList.getInstance();
		for (int i = 0; i < List.size();i++) {
			if (List.get(i).getCarId() == vehicle.getCarId()) {
				List.remove(i);
				List.add(vehicle);
				return Response.status(Response.Status.OK).build();
			}


		}
		return Response.status(Response.Status.NOT_FOUND).build();      

	}       
	
	//Adds a vehicle to the database.
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCustomer(Car vehicle){
		SortedList<Car> List = CarList.getInstance();
		List.add(vehicle);
		return Response.status(201).build();
	} 

	//Gives list with all the cars in it.
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCustomers() {
		SortedList<Car> List = CarList.getInstance();
		Car[] temp = new Car[List.size()];
		for (int i = 0; i < List.size(); i++) temp[i] = List.get(i);
		
		return temp;
	}  
	
	//Finds a customer using his car's ID.
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCustomer(@PathParam("id") long id) {
		SortedList<Car> List = CarList.getInstance();
		for (int i= 0; i < List.size(); i++) {
			if (List.get(i).getCarId() == id) return List.get(i);

		}
		throw new NotFoundException();
	}   

}