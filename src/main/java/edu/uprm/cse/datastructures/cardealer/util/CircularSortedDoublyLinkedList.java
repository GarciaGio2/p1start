package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E>{
	
	private int currentSize;
	private Node<E> head;
	private Comparator<E> comp;
	
	/**
	 * Constructs the list
	 * 
	 * @param comparator
	 * @return None
	 */

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		head = new Node<E>(null, head, head);
		this.head.setElement(null);
		this.head.setNext(head);
		this.head.setPrev(head);
		currentSize = 0;
		comp = comparator;
	}
	
	/**
	 * Iterates through the list
	 * 
	 * @param Node
	 * @return the list's iterator
	 */
	
	@Override
	public Iterator<E> iterator() {
		List<E> newList = new ArrayList<E>();
		Node<E> newNode = this.head.getNext();
		while(newNode != head) {
			newList.add(newNode.getElement());
			newNode = newNode.getNext();
		}
		return newList.iterator();
	}
	
	/**
	 * Adds an object to the list
	 * 
	 * @param Node element
	 * @return returns true when object is added.
	 */

	@Override
	public boolean add(E obj) {
		if(this.isEmpty()){
			Node<E> n = new Node<E>(obj, this.head, this.head);
			this.head.setNext(n);
			this.head.setPrev(n);
			this.currentSize++;
			return true;
		}
		else{
			Node<E> n = head.getNext();
			while(n.getElement()!=null) {
				if(this.comp.compare(obj, n.getElement())<=0) {
					Node<E> newNode = new Node<E>(obj, n, n.getPrev());
					n.getPrev().setNext(newNode);
					n.setPrev(newNode);
					currentSize++;
					return true;
				}
				n=n.getNext();
			}
			Node<E> newNode = new Node<E>(obj, this.head, this.head.getPrev());
			this.head.getPrev().setNext(newNode);
			this.head.setPrev(newNode);
			currentSize++;
			return true;
		}
	}
	/**
	 * gets the size of the list
	 * 
	 * @param None
	 * @return returns the size of the list
	 */

	@Override
	public int size() {
		return currentSize;
	}
	
	/**
	 * Removes specified object .
	 * 
	 * @param Node element
	 * @return true when executed.
	 */
	
	@Override
	public boolean remove(E obj) {
		int num = 0;
		for (Node<E> n1 = this.head.next;!(n1.getElement() == null);num++) {
			if (n1.getElement().equals(obj)) break;
			else {
				n1 = n1.getNext();
				
				continue;
			}
		}
		if (num == currentSize) num = -1;
		
		if(num >= 0) {
			this.remove(num);
			return true;
		}
		return false;
	}
	
	/**
	 * Removes an element based on the given index.
	 * 
	 * @param Index
	 * @return true when item is removed
	 */
	
	@Override
	public boolean remove(int index) {
		if(index > this.size() || index < -1) throw new IndexOutOfBoundsException("Index is out of bounds");
		else {
			Node<E> temp = this.head;
			int i = 0;
			while(i < index) {
				temp = temp.getNext();
				i++;
			}
			Node<E> target = temp.getNext();
			temp.setNext(target.getNext());
			target.getNext().setPrev(temp);
			target = null;
			this.currentSize--;
			return true;
			
		}
	}
	/**
	 * Removes all the nodes with the same value as the object.
	 * 
	 * @param Node element
	 * @return quantity of items removed.
	 */

	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj) != false) {
			count++;
		}
		return count;
	}
	
	/**
	 * Finds the object with the given index.
	 * 
	 * @param index
	 * @return returns the object that has that index.
	 */
	
	@Override
	public E get(int index) {
		Node<E> n1 = this.head.getNext();
		int num = 0;
		while(num < index) {
			n1 = n1.getNext();
			num++;
		}
		return n1.getElement();
	}
	
	/**
	 * Clears the list
	 * 
	 * @param None
	 * @return None
	 */
	
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}
	
	/**
	 * Checks if the given node is on the list.
	 * 
	 * @param Node element
	 * @return returns true if the given node is in the list, false if its not.
	 */
	
	@Override
	public boolean contains(E e) {
		if(this.firstIndex(e) > -1) return true;
		
		return false;
	}
	
	/**
	 * Finds if the list is empty.
	 * 
	 * @param None
	 * @return returns true if the list is empty, false if its not.
	 */
	
	@Override
	public boolean isEmpty() {
		if (currentSize == 0) return true;
		return false;
	}

	/**
	 * Returns the first index of the given element.
	 * 
	 * @param Node element
	 * @return returns the first index of the parameter object, if its not 
	 * present it returns -1;
	 */
	
	@Override
	public int firstIndex(E e) {
		int num = 0;
		for (Node<E> n1 = this.head.next;!(n1.getElement() == null);num++) {
			if (n1.getElement().equals(e)) {
				return num;
			}
			n1 = n1.getNext();
		}
		return -1;
	}
	
	/**
	 * Returns the first element of the list.
	 * 
	 * @param None
	 * @return returns first element, if its not present, it returns null.
	 */
	@Override
	public E first() {
		if (!this.isEmpty()) return this.head.getNext().getElement();
		return null;
	}
	
	/**
	 * Returns the last index of the given element.
	 * 
	 * @param Node element
	 * @return returns the last index of the parameter object, if not present,
	 * it returns -1.
	 */
	@Override
	public int lastIndex(E e) {
		int num = 0;
		int n1 = -1;
		for (Node<E> temp = this.head.getNext();temp.getElement() != null;num++) {
			if (temp.getElement().equals(e)) n1 = num;
			
			temp = temp.getNext();
		}
		return n1;
	}
	
	/**
	 * Returns the last element of the list.
	 * 
	 * @param None
	 * @return last element, if its not present, it returns null.
	 */
	@Override
	public E last() {
		if (!this.isEmpty()) return this.head.getPrev().getElement();
		return null;
	}
	
	private class Node<E>{
		private E element;
		private Node<E> next, prev;
		public Node(E element, Node<E> next, Node<E> prev){
			this.element=element;
			this.next=next;
			this.prev=prev;
		}
		public E getElement(){
			return element;
		}
		public void setElement(E element){
			this.element=element;
		}
		public Node<E> getNext(){
			return next;
		}
		public Node<E> getPrev(){
			return prev;
		}
		public void setNext(Node<E> next){
			this.next=next;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}
}